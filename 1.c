#include <stdio.h>
#include <stdlib.h>

void pattern(int p);
void Row(int q);
int Nrows=1;


void pattern(int p)
{

    if(p>0)
    {
        Row(Nrows);
        printf("\n");
        Nrows++;
        pattern(p-1);

    }
}


void Row(int q)
{
    if(q>0)
    {
        printf("%d",q);
        Row(q-1);
    }
}

int main()
{int x;

    printf("Enter the number of Rows :");
    scanf("%d",&x);
    pattern(x);
    return 0;
}
